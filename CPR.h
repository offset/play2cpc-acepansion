/*
** CPR file management
*/

#ifndef CPR_H
#define CPR_H



#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif



struct CartridgeImageData
{
    UBYTE *raw;           // Cartridge RAW data
    UBYTE *rom[32];       // ROM pages address space

    APTR  *extROM;        // Extended ROM pages addresse space (NULL if not extended cartridge)
    USHORT extCount;      // Number of 512KB pages
    USHORT extPage;       // Current 512KB page
    UBYTE  extWritePhase; // Current SST write phase
    BOOL   extModified;   // Flash was updated from CPU
};



struct CartridgeImageData * LoadCPR(CONST_STRPTR fileName);
BOOL SaveCPR(struct CartridgeImageData *image, CONST_STRPTR fileName);
VOID FreeCPR(struct CartridgeImageData *image);

UBYTE ReadCPR(struct CartridgeImageData *image, UBYTE bank, USHORT address);
VOID WriteCPR(struct CartridgeImageData *image, UBYTE bank, USHORT address, UBYTE value);

#endif
