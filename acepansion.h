/*
** play2cpc.acepansion public API
*/

#ifndef ACEPANSION_H
#define ACEPANSION_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef ACEPANSION_PLUGIN_H
#include <acepansion/plugin.h>
#endif


#define LIBNAME "play2cpc.acepansion"
#define VERSION 1
#define REVISION 1
#define DATE "21.10.2023"
#define COPYRIGHT "� 2022-2023 Philippe Rimauro"

#define API_VERSION 7


/*
** Play2CPC plugin specific API to be called from GUI
*/
VOID Plugin_SetCartridgeFileName(struct ACEpansionPlugin *plugin, CONST_STRPTR fileName);


#endif /* ACEPANSION_H */

