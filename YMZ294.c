#include "YMZ294.h"

static const ULONG VolumesBase[] = { 0, 231, 695, 1158, 2084, 2779, 4168, 6716, 8105, 13200, 18294, 24315, 32189, 40757, 52799, 65535 };

VOID YMInit(struct YMData *data)
{
    YMReset(data);
}

VOID YMReset(struct YMData *data)
{
    LONG i;

    for(i=0; i<16; i++)
    {
        data->ym_Register[i] = 0;
    }

    data->ym_ChannelAFreq = 1;
    data->ym_ChannelBFreq = 1;
    data->ym_ChannelCFreq = 1;

    data->ym_NoiseShiftRegister = 1;

    data->ym_NoiseFrequency = 1 << 4;
    data->ym_MixerControlRegister = 0xFF;

    data->ym_ChannelAVolume = 0;
    data->ym_ChannelBVolume = 0;
    data->ym_ChannelCVolume = 0;

    data->ym_VolumeEnveloppeFrequency = 1 << 4;
    data->ym_VolumeEnveloppeShape = 0;

    data->ym_CounterA = 0;
    data->ym_CounterB = 0;
    data->ym_CounterC = 0;

    data->ym_ChannelAHigh = 1;
    data->ym_ChannelBHigh = 1;
    data->ym_ChannelCHigh = 1;
    data->ym_ChannelNoiseHigh = 1;

    data->ym_CounterNoise = 0;
    data->ym_CounterEnv = 0;
    data->ym_CounterStateEnv = 0;
    data->ym_EnveloppeVolume = 0;
    data->ym_Stop = FALSE;

    data->ym_RegisterAddress = 0;

    data->ym_OutputA =
    data->ym_OutputB =
    data->ym_OutputC = 0;
}

VOID YMAccess(struct YMData *data, UBYTE *data_bus, UBYTE bus_ctrl)
{
    // Depends on the BusCTRL
    if(bus_ctrl & 0x2)     // BDIR = 1
    {
        if(bus_ctrl & 0x1)
        {
            // Write address
            data->ym_RegisterAddress = *data_bus;
        }
        else
        {
            switch(data->ym_RegisterAddress)
            {
                case 0x00: data->ym_Register[data->ym_RegisterAddress] = *data_bus; data->ym_ChannelAFreq = ((data->ym_ChannelAFreq & 0xff00) + *data_bus); data->ym_ChannelAFreqCounter = (data->ym_ChannelAFreq << YM_CLOCK_DIV); break;
                case 0x01: data->ym_Register[data->ym_RegisterAddress] = *data_bus & 0x0F; data->ym_ChannelAFreq = ((data->ym_ChannelAFreq & 0xff) + ((*data_bus & 0xF) << 8)); data->ym_ChannelAFreqCounter = (data->ym_ChannelAFreq << YM_CLOCK_DIV); break;
                case 0x02: data->ym_Register[data->ym_RegisterAddress] = *data_bus; data->ym_ChannelBFreq = ((data->ym_ChannelBFreq & 0xff00) + *data_bus); data->ym_ChannelBFreqCounter = (data->ym_ChannelBFreq << YM_CLOCK_DIV); break;
                case 0x03: data->ym_Register[data->ym_RegisterAddress] = *data_bus & 0x0F; data->ym_ChannelBFreq = ((data->ym_ChannelBFreq & 0xff) + ((*data_bus & 0xF) << 8)); data->ym_ChannelBFreqCounter = (data->ym_ChannelBFreq << YM_CLOCK_DIV); break;
                case 0x04: data->ym_Register[data->ym_RegisterAddress] = *data_bus; data->ym_ChannelCFreq = ((data->ym_ChannelCFreq & 0xff00) + *data_bus); data->ym_ChannelCFreqCounter = (data->ym_ChannelCFreq << YM_CLOCK_DIV); break;
                case 0x05: data->ym_Register[data->ym_RegisterAddress] = *data_bus & 0xF; data->ym_ChannelCFreq = ((data->ym_ChannelCFreq & 0xff) + ((*data_bus & 0xF) << 8)); data->ym_ChannelCFreqCounter = (data->ym_ChannelCFreq << YM_CLOCK_DIV); break;
                case 0x06: data->ym_Register[data->ym_RegisterAddress] = *data_bus & 0x1F; data->ym_NoiseFrequency = ((*data_bus == 0) ? 1 : (*data_bus & 0x1F))<< YM_CLOCK_DIV; break;
                case 0x07: data->ym_Register[data->ym_RegisterAddress] = *data_bus; data->ym_MixerControlRegister = *data_bus; break;
                case 0x08: data->ym_Register[data->ym_RegisterAddress] = *data_bus & 0x1F; data->ym_ChannelAVolume = *data_bus & 0x1F; break;
                case 0x09: data->ym_Register[data->ym_RegisterAddress] = *data_bus & 0x1F; data->ym_ChannelBVolume = *data_bus & 0x1F; break;
                case 0x0A: data->ym_Register[data->ym_RegisterAddress] = *data_bus & 0x1F; data->ym_ChannelCVolume = *data_bus & 0x1F; break;
                case 0x0B: data->ym_Register[data->ym_RegisterAddress] = *data_bus; data->ym_VolumeEnveloppeFrequency = ((data->ym_Register[0x0B]) + (data->ym_Register[0x0C] << 8)) << YM_CLOCK_DIV; break;
                case 0x0C: data->ym_Register[data->ym_RegisterAddress] = *data_bus; data->ym_VolumeEnveloppeFrequency = ((data->ym_Register[0x0B]) + (data->ym_Register[0x0C] << 8)) << YM_CLOCK_DIV; break;
                case 0x0D: data->ym_Register[data->ym_RegisterAddress] = *data_bus & 0x0F; data->ym_VolumeEnveloppeShape = (*data_bus & 0xF); data->ym_CounterEnv = 0; data->ym_Stop = FALSE; data->ym_Up = ((data->ym_VolumeEnveloppeShape & 0x04) == 0x04); data->ym_EnveloppeVolume = (data->ym_Up) ? 0 : 15; break;
            }
            // Correction :
            if (data->ym_VolumeEnveloppeFrequency == 0) data->ym_VolumeEnveloppeFrequency = 1 << 2 << YM_CLOCK_DIV;
            if (data->ym_ChannelAFreq == 0) { data->ym_ChannelAFreq = 1; data->ym_ChannelAFreqCounter = (data->ym_ChannelAFreq << YM_CLOCK_DIV); }
            if (data->ym_ChannelBFreq == 0) { data->ym_ChannelBFreq = 1; data->ym_ChannelBFreqCounter = (data->ym_ChannelBFreq << YM_CLOCK_DIV); }
            if (data->ym_ChannelCFreq == 0) { data->ym_ChannelCFreq = 1; data->ym_ChannelCFreqCounter = (data->ym_ChannelCFreq << YM_CLOCK_DIV); }
        }
    }
    else                   // BDIR = 0
    {
        if(bus_ctrl & 0x01)
        {
            // Lire le registre du psg.
            // Exemple : Registre 14 (clavier) , on retourne un truc pour voir
            switch (data->ym_RegisterAddress)
            {
                case 0x00:
                case 0x01:
                case 0x02:
                case 0x03:
                case 0x04:
                case 0x05:
                case 0x06:
                case 0x07:
                case 0x08:
                case 0x09:
                case 0x0A:
                case 0x0B:
                case 0x0C:
                    *data_bus = data->ym_Register[data->ym_RegisterAddress];
                    break;
                case 0x0D:
                    *data_bus = data->ym_VolumeEnveloppeShape;
                    break;
            }
        }
    }
}

VOID YMTick(struct YMData *data)
{
    // Tone generator

    // One more clock tick
    // Count each Chan : Do we have to invert square output ?
    ++data->ym_CounterA;
    ++data->ym_CounterB;
    ++data->ym_CounterC;

    if(data->ym_CounterA >= data->ym_ChannelAFreqCounter)
    {
        // Inversion !
        data->ym_ChannelAHigh ^= 1;
        data->ym_CounterA = 0;
    }
    if(data->ym_CounterB >= data->ym_ChannelBFreqCounter)
    {
        // Inversion !
        data->ym_ChannelBHigh ^= 1;
        data->ym_CounterB = 0;
    }
    if(data->ym_CounterC >= data->ym_ChannelCFreqCounter)
    {
        // Inversion !
        data->ym_ChannelCHigh ^= 1;
        data->ym_CounterC = 0;
    }

    // Noise
    data->ym_CounterNoise++;
    if(data->ym_CounterNoise >= data->ym_NoiseFrequency
    && (data->ym_CounterNoise & 1))
    {
        // Inversion !
        data->ym_CounterNoise = 0;

        if((data->ym_NoiseShiftRegister + 1) & 2)
        {
            data->ym_ChannelNoiseHigh ^= 1;
        }

        if(data->ym_NoiseShiftRegister & 1)
        {
            data->ym_NoiseShiftRegister ^= 0x24000;
        }
        data->ym_NoiseShiftRegister >>= 1;
    }

    // Enveloppe - *2 to map 16 step instead of 32 (todo : fix it)
    if(data->ym_CounterEnv >= (data->ym_VolumeEnveloppeFrequency*2))
    {
        data->ym_CounterEnv = 0;
        if(data->ym_Stop)
        {
            // Hold or continue;
        }
        else
        {
            // One tick of the enveloppe
            data->ym_CounterStateEnv++;

            // End ?
            if(data->ym_CounterStateEnv >= 16 )
            {
                data->ym_CounterStateEnv = 0;
                // Continue
                if((data->ym_VolumeEnveloppeShape & 0x08) == 0x00)
                {
                    data->ym_EnveloppeVolume = 0;
                    data->ym_Stop = TRUE;
                }
                else
                {
                    // Alternate ?
                    if((data->ym_VolumeEnveloppeShape & 0x02) == 0x02)
                    {
                        data->ym_Up = !data->ym_Up;
                    }
                    else
                    {
                        data->ym_EnveloppeVolume = data->ym_Up ? 0 : 15;
                    }

                    // Hold
                    if((data->ym_VolumeEnveloppeShape & 0x01) == 0x01)
                    {
                        data->ym_Stop = TRUE;

                        if(data->ym_Up)
                        {
                            data->ym_EnveloppeVolume = 15;
                        }
                        else
                        {
                            data->ym_EnveloppeVolume = 0;
                        }
                    }
                }
            }
            else
            {
                // Normal step
                // Attack
                if(data->ym_Up)
                {
                    data->ym_EnveloppeVolume++;
                }
                else
                {
                    data->ym_EnveloppeVolume--;
                }
            }
        }
    }
    data->ym_EnveloppeVolume &= 0xF;
    ++data->ym_CounterEnv;

    LONG add_a = -1;
    BYTE mix_a = 1;
    if((data->ym_MixerControlRegister & 0x1) == 0) mix_a &= data->ym_ChannelAHigh;
    if((data->ym_MixerControlRegister & 0x8) == 0) mix_a &= data->ym_ChannelNoiseHigh;

    if(mix_a)
    {
        add_a = (data->ym_ChannelAVolume < 0x10) ? data->ym_ChannelAVolume : data->ym_EnveloppeVolume;
    }
    else
    {
        add_a = ((data->ym_ChannelAVolume < 0x10) ? /*(data->ym_ChannelAHigh ?0:(-1 * data->ym_ChannelAVolume ))*/0 : data->ym_EnveloppeVolume);
    }

    LONG add_b = -1;
    BYTE mix_b = 1;
    if((data->ym_MixerControlRegister & 0x2) == 0)  mix_b &= data->ym_ChannelBHigh;
    if((data->ym_MixerControlRegister & 0x10) == 0) mix_b &= data->ym_ChannelNoiseHigh;

    if(mix_b)
    {
        add_b = ((data->ym_ChannelBVolume < 0x10) ? data->ym_ChannelBVolume/*(data->ym_ChannelBHigh ? 0 : (-1 * data->ym_ChannelBVolume)) */ : data->ym_EnveloppeVolume);
    }
    else
    {
        add_b = ((data->ym_ChannelBVolume<0x10) ? 0 : data->ym_EnveloppeVolume);
    }

    LONG add_c = -1;
    BYTE mix_c = 1;
    if((data->ym_MixerControlRegister & 0x4) == 0)  mix_c &= data->ym_ChannelCHigh;
    if((data->ym_MixerControlRegister & 0x20) == 0) mix_c &= data->ym_ChannelNoiseHigh;
    if(mix_c)
    {
        add_c = ((data->ym_ChannelCVolume < 0x10) ? data->ym_ChannelCVolume/*(data->ym_ChannelCHigh ? 0 : (-1 * data->ym_ChannelCVolume ))*/ : data->ym_EnveloppeVolume);
    }
    else
    {
        add_c =((data->ym_ChannelCVolume<0x10) ? 0 : data->ym_EnveloppeVolume);
    }

    data->ym_OutputA = VolumesBase[add_a];
    data->ym_OutputB = VolumesBase[add_b];
    data->ym_OutputC = VolumesBase[add_c];
}

