/*
** CPR file management
*/

#include <clib/alib_protos.h>
#include <clib/debug_protos.h>

#include <hardware/byteswap.h>
#include <proto/exec.h>
#include <proto/dos.h>

#include <string.h>

#include "CPR.h"



extern struct Library *SysBase;
extern struct Library *DOSBase;



struct RIFFChunk
{
    UBYTE id[4];
    ULONG size;
};

struct CPRHeader
{
    struct RIFFChunk chunk;
    UBYTE signature[4];
};



struct CartridgeImageData * LoadCPR(CONST_STRPTR fileName)
{
    BPTR file;
    struct CartridgeImageData *image = NULL;
    BOOL failed = FALSE;

    kprintf("File: %s\n", fileName);

    if((file = Open(fileName, MODE_OLDFILE)) == (BPTR)0)
    {
        failed = TRUE;
    }
    else
    {
        LONG pagesPerSlot = -1;
        LONG lastPage = -1;
        BOOL foundBootPage = FALSE;
        ULONG pass;

        for(pass=0; pass<2 && !failed; pass++)
        {
            struct CPRHeader header;

            // Check the master chunk
            if(Read(file, &header, sizeof(struct CPRHeader)) != sizeof(struct CPRHeader))
            {
                failed = TRUE;
            }
            else
            {
                BOOL isCPR = header.signature[0] == 'A'
                          && header.signature[1] == 'M'
                          && header.signature[2] == 'S'
                          && header.signature[3] == '!';
                BOOL isExtendedCPR = header.signature[0] == 'C'
                                  && header.signature[1] == 'X'
                                  && header.signature[2] == 'M'
                                  && header.signature[3] == 'E';

                header.chunk.size = BE_SWAPLONG(header.chunk.size);

                kprintf("Found chunk of size: %ld\n", header.chunk.size);
                kprintf("Id: %lc%lc%lc%lc\n", header.chunk.id[0], header.chunk.id[1], header.chunk.id[2], header.chunk.id[3]);
                kprintf("Content: %lc%lc%lc%lc\n", header.signature[0], header.signature[1], header.signature[2], header.signature[3]);
                // Check if it is valid RIFF Cartridge file
                if(header.chunk.id[0] != 'R'
                || header.chunk.id[1] != 'I'
                || header.chunk.id[2] != 'F'
                || header.chunk.id[3] != 'F'
                || (!isCPR && !isExtendedCPR))
                {
                    failed = TRUE;
                }
                else
                {
                    kprintf("Valid CPR\n");

                    if(pass == 0)
                    {
                        image = (struct CartridgeImageData *)AllocVec(sizeof(struct CartridgeImageData), MEMF_PUBLIC|MEMF_CLEAR);
                    }

                    if(image == NULL)
                    {
                        failed = TRUE;
                    }
                    else
                    {
                        // We can look for chunks!
                        LONG pageCount = 0;
                        ULONG currentSize = 4;
                        LONG i;

                        while(currentSize < header.chunk.size)
                        {
                            struct RIFFChunk chunk;

                            if(Read(file, &chunk, sizeof(struct RIFFChunk)) != sizeof(struct RIFFChunk))
                            {
                                failed = TRUE;
                            }
                            else
                            {
                                LONG oldSeek = Seek(file, 0, OFFSET_CURRENT);

                                chunk.size = BE_SWAPLONG(chunk.size);
                                kprintf("Found chunk of size: %ld\n", chunk.size);

                                if((isCPR && chunk.id[0] == 'c' && chunk.id[1] == 'b'
                                          && chunk.id[2] >= '0' && chunk.id[2] <= '9'
                                          && chunk.id[3] >= '0' && chunk.id[3] <= '9')
                                || (isExtendedCPR && chunk.id[0] == 'C' && chunk.id[1] == 'X'))
                                {
                                    // Valid chunk: analyze its content
                                    LONG page;

                                    if(isExtendedCPR)
                                        page = chunk.id[2] << 8 | chunk.id[3];
                                    else
                                        page = (chunk.id[2] - '0') * 10 + (chunk.id[3] - '0');

                                    if(isExtendedCPR || page < 32)
                                    {
                                        switch(pass)
                                        {
                                            case 0:
                                                kprintf("It is a cb?? or CX?? chunk! Page %ld at offset %ld (checking)\n", page, Seek(file, 0, OFFSET_CURRENT));

                                                if(page > lastPage)
                                                {
                                                    lastPage = page;
                                                }

                                                if(page == 0)
                                                {
                                                    foundBootPage = TRUE;
                                                }
                                                break;
                                            case 1:
                                                // Load the pages into the RAW memory
                                                kprintf("It is a cb?? or CX?? chunk! Page %ld at offset %ld (loading)\n", page, Seek(file, 0, OFFSET_CURRENT));

                                                Read(file, image->raw + (pageCount*0x4000), chunk.size > 0x4000 ? 0x4000 : chunk.size);

                                                if(chunk.size != 0x4000)
                                                {
                                                    kprintf("Warning: chunk with size &%04lx instead of &40000 was truncated/expanded\n", chunk.size);
                                                }
                                                // Configure the address space of the available ROMs
                                                if(page < 32)
                                                {
                                                    image->rom[page] = image->raw + pageCount*0x4000;
                                                }
                                                if(image->extROM)
                                                {
                                                    image->extROM[page] = image->raw + pageCount*0x4000;
                                                }
                                                kprintf("Load ROM %ld = 0x%08lx (%lx)\n", page, &image->rom[page], chunk.size);
                                                break;
                                        }
                                        pageCount++;
                                    }
                                }

                                if(isExtendedCPR && chunk.id[0] == 'N'
                                                 && chunk.id[1] == 'B'
                                                 && chunk.id[2] == 'B'
                                                 && chunk.id[3] == 'K'
                                                 && chunk.size == 2)
                                {
                                    if(pass == 1)
                                    {
                                        USHORT nbbk;

                                        Read(file, &nbbk, 2);

                                        pagesPerSlot = LE_SWAPWORD(nbbk);
                                        kprintf("Obtained pages per slot from NBBK chunk: %ld\n", pagesPerSlot);
                                    }
                                }

                                 // Go to the next chunk
                                Seek(file, oldSeek, OFFSET_BEGINNING);
                                Seek(file, chunk.size, OFFSET_CURRENT);

                                currentSize += 8 + chunk.size;

                            }
                        }
                        switch(pass)
                        {
                            case 0:
                                kprintf("Found %ld pages\n", lastPage+1);
                                if(currentSize != header.chunk.size)
                                {
                                    failed = TRUE;
                                }
                                else
                                {
                                    if(!foundBootPage || lastPage == -1)
                                    {
                                        failed = TRUE;
                                    }
                                    else
                                    {
                                        LONG lastHardPage = (lastPage+0x20) & ~0x1f;

                                        // Read the CPR content
                                        if((image->raw = (UBYTE *)AllocVec(pageCount*0x4000                                 /* Actual ROMs */
                                                                         + 0x4000                                           /* Empty space for missing ROMs */
                                                                         + (isExtendedCPR ? lastHardPage*sizeof(APTR) : 0), /* Extended ROMs index */
                                                                         MEMF_PUBLIC|MEMF_CLEAR)) == NULL)
                                        {
                                            failed = TRUE;
                                        }
                                        else
                                        {
                                            // Default 512KB page (only changed by extended cartridges)
                                            image->extROM = NULL;
                                            image->extPage = 0;
                                            image->extCount = lastHardPage / 32;

                                            // Guess cart size from last page chunk
                                            if(lastPage > 15)
                                                pagesPerSlot = 32;
                                            else if(lastPage > 7)
                                                pagesPerSlot = 16;
                                            else if(lastPage > 3)
                                                pagesPerSlot = 8;
                                            else if(lastPage > 1)
                                                pagesPerSlot = 4;
                                            else if(lastPage > 0)
                                                pagesPerSlot = 2;
                                            else
                                                pagesPerSlot = 1;

                                            // All ROMs default to empty space
                                            for(i=0; i<32; i++)
                                            {
                                                // Configure the address space of the non-available ROMs
                                                image->rom[i] = image->raw + pageCount*0x4000;
                                            }
                                            if(isExtendedCPR)
                                            {
                                                // Extended ROMs index
                                                image->extROM = (void**)(image->raw + pageCount*0x4000 + 0x4000);

                                                for(i=0; i<lastHardPage; i++)
                                                {
                                                    // Configure the address space of the non-available ROMs
                                                    image->extROM[i] = image->raw + pageCount*0x4000;
                                                }
                                            }
                                            // Ready for 2nd pass (actual loading)
                                            Seek(file, 0, OFFSET_BEGINNING);

                                            kprintf("%s with %ld 512KB pages (%ld 16K pages allocated)\n", image->extROM ? "Cartridge" : "Extended cartridge", image->extCount, pageCount);
                                        }
                                    }
                                }
                                break;

                            case 1:
                                for(i=pagesPerSlot; i<32; i++)
                                {
                                    kprintf("Configuring mirror page %ld/%ld\n", i, pagesPerSlot);
                                    image->rom[i] = image->rom[i % pagesPerSlot];
                                }
                                if(isExtendedCPR)
                                {
                                    LONG slotCount = image->extCount/32;
                                    LONG slot;

                                    for(slot=0; slot<slotCount; slot++)
                                    {
                                        for(i=0; i<32; i++)
                                        {
                                            image->extROM[slot*32 + i] = image->extROM[slot*32 + (i % pagesPerSlot)];
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }
        Close(file);
    }

    if(failed)
    {
        // Free incomplete image...
        FreeCPR(image);
        image = NULL;
    }

    return image;
}



BOOL SaveCPR(struct CartridgeImageData *image, CONST_STRPTR fileName)
{
    BOOL success = FALSE;

    // Only extended cartridges can be self updated
    if(image->extROM)
    {
        BPTR file = Open(fileName, MODE_NEWFILE);

        if(file)
        {
            LONG page, bank;
            LONG chunkSize = sizeof(struct CPRHeader) -  sizeof(struct RIFFChunk)
                           + image->extCount * (0x80000 + 32 * sizeof(struct RIFFChunk));

            struct CPRHeader header = { { {'R', 'I', 'F', 'F'}, BE_SWAPLONG(chunkSize) }, {'C','X','M','E' } };

            if(Write(file, &header, sizeof(header)) != sizeof(header))
            {
                goto exit;
            }

            for(page=0; page<image->extCount; page++)
            {
                for(bank=0; bank<32; bank++)
                {
                    USHORT id = page * 32 + bank;
                    APTR ptr = &image->raw[id * 0x4000];

                    struct RIFFChunk chunk =
                    {
                        { 'C', 'X', id >> 8, id },
                        BE_SWAPLONG(0x4000)
                    };

                    if(Write(file, &chunk, sizeof(chunk) != sizeof(chunk)))
                    {
                        goto exit;
                    }
                    if(Write(file, ptr, 0x4000) != 0x4000)
                    {
                        goto exit;
                    }
                }
            }

            success = TRUE;
exit:
            Close(file);
        }
    }

    return success;
}




VOID FreeCPR(struct CartridgeImageData *image)
{
    if(image)
    {
        FreeVec(image->raw);
        FreeVec(image);
    }
}



UBYTE ReadCPR(struct CartridgeImageData *image, UBYTE bank, USHORT address)
{
    // Handle extented cartridges (up to 1.5MB, SST write support)
    if(image->extROM)
    {
        if(bank == 0)
        {
            LONG page = 0x3fff - address;

            if(page < image->extCount)
            {
                if(image->extPage != page)
                {
                    // New current 512KB page
                    image->extPage = page;
                    // Update cartridge ROM table
                    CopyMem(&image->extROM[image->extPage*32], image->rom, 32*sizeof(APTR));
                }
            }
        }
    }

    return image->rom[bank][address];
}



VOID WriteCPR(struct CartridgeImageData *image, UBYTE bank, USHORT address, UBYTE value)
{
    // Handle extented cartridges (up to 1.5MB, SST write support)
    if(image->extROM)
    {
        LONG flashAddress = image->extPage << 19 | (bank << 14) | address;

        // Flash is organized as 128 sector of 4K and a special sequence is
        // required to determine which sector should be erases then written

        kprintf("XPR Write detected phase: %2ld (value=&%02lx, flashAddress=0x%08lx)\n", image->extWritePhase, value, flashAddress);

        // Erase/Write unlock
             if(image->extWritePhase == 0 && value == 0xaa && flashAddress == 0x5555) { image->extWritePhase++; }
        else if(image->extWritePhase == 1 && value == 0x55 && flashAddress == 0x2aaa) { image->extWritePhase++; }
        // Erase
        else if(image->extWritePhase == 2 && value == 0x80 && flashAddress == 0x5555) { image->extWritePhase = 8; }
        else if(image->extWritePhase == 8 && value == 0xaa && flashAddress == 0x5555) { image->extWritePhase++; }
        else if(image->extWritePhase == 9 && value == 0x55 && flashAddress == 0x2aaa) { image->extWritePhase++; }
        // Actual erase
        else if(image->extWritePhase == 10 && value == 0x30)
        {
            memset(&image->rom[bank][address & ~0xfff], 0x00, 0x1000);
            image->extWritePhase = 0;
            image->extModified = TRUE;
        }
        // Write
        else if(image->extWritePhase == 2 && value == 0xa0 && flashAddress == 0x5555) { image->extWritePhase = 16; }
        // Actual write
        else if(image->extWritePhase == 16)
        {
            image->rom[bank][address] |= value;
            image->extWritePhase = 0;
            image->extModified = TRUE;
        }
        // In any other cas we cancel write sequence
        else
        {
            image->extWritePhase = 0;
        }
    }
}

