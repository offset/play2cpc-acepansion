/*
** Graphical User Interface using the Be API
** for Play2CPC ACEpansion
*/

#include <Alert.h>
#include <Box.h>
#include <Button.h>
#include <GroupView.h>
#include <LayoutBuilder.h>
#include <String.h>

#include "sys/haiku/PopASL.h"

#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */

class ControlBox: public BBox
{
    public:
        ControlBox(struct ACEpansionPlugin* plugin)
            : BBox("control box")
            , myPlugin(plugin)
        {
            SetLabel(GetString(MSG_PREFS));

            BGroupView* inner = new BGroupView(B_HORIZONTAL);
            AddChild(inner);

            BLayoutBuilder::Group<>(inner)
                .SetInsets(B_USE_DEFAULT_SPACING)
                .Add(STR_Cartridge = new PopASL(0, GetString(MSG_CARTRIDGE), NULL, "cartridge",
                    "#?.(cpr|xpr)", GetString(MSG_CARTRIDGE_HELP)))
                .Add(BTN_Eject = new BButton("eject", GetString(MSG_EJECT_BUTTON), new BMessage('EJCT')))
            .End();

            STR_Cartridge->SetMessage(new BMessage('CART'));
        }

        void AttachedToWindow()
        {
            BBox::AttachedToWindow();


            STR_Cartridge->SetTarget(this);
            BTN_Eject->SetTarget(this);
        }

        void MessageReceived(BMessage* message)
        {
            switch(message->what)
            {
                case 'CART':
                {
                    BString name = message->FindString("Name");
					Plugin_SetCartridgeFileName(myPlugin, name);
                    break;
                }
                case 'EJCT':
					Plugin_SetCartridgeFileName(myPlugin, NULL);
                    break;
                default:
                    BBox::MessageReceived(message);
                    break;
            }
        }

        PopASL* STR_Cartridge;
    private:
        BButton* BTN_Eject;
        struct ACEpansionPlugin* myPlugin;
};



/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
	return TRUE;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
}



/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object * GUI_Create(struct ACEpansionPlugin *plugin)
{
    return (Object*)new ControlBox(plugin);
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
	BView* view = (BView*)gui;
	BWindow* window = view->Window();
	window->Lock();
	view->RemoveSelf();
	window->Unlock();
	delete view;
}



/*
** Functions called from the plugin
*/
VOID GUI_SetCartridgeFileName(Object *gui, CONST_STRPTR fileName)
{
	ControlBox* top = (ControlBox*)gui;
	bool unlock = top->LockLooper();
	top->STR_Cartridge->TextControl()->SetText(fileName);
	if (unlock) top->UnlockLooper();
}

VOID GUI_NotifyCartridgeLoadError(UNUSED Object *gui, CONST_STRPTR fileName)
{
	BString message;
	message.SetToFormat(GetString(MSG_CARTRIDGE_LOAD_FAILED_S), fileName);
	BAlert alert(GetString(MSG_TITLE), message.String(), GetString(MSG_OK), NULL, NULL,
		B_WIDTH_AS_USUAL, B_WARNING_ALERT);
	alert.Go();
}

VOID GUI_NotifyCartridgeSaveError(UNUSED Object *gui, CONST_STRPTR fileName)
{
	BString message;
	message.SetToFormat(GetString(MSG_CARTRIDGE_SAVE_FAILED_S), fileName);
	BAlert alert(GetString(MSG_TITLE), message.String(), GetString(MSG_OK), NULL, NULL,
		B_WIDTH_AS_USUAL, B_WARNING_ALERT);
	alert.Go();
}



