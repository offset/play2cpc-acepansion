/*
** Graphical User Interface using MUI
** for Play2CPC ACEpansion
*/

#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/muimaster.h>

#include <libraries/asl.h>
#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



struct Library *IntuitionBase;
struct Library *UtilityBase;
struct Library *MUIMasterBase;



struct MUI_CustomClass *MCC_Play2CPCClass = NULL;



/*
** MCC_Play2CPCClass definitions
*/
#define Play2CPCObject NewObject(MCC_Play2CPCClass->mcc_Class, NULL

#define MUIA_Play2CPC_Plugin (TAG_USER | 0x0000) // [I..] struct ACEpansionPlugin *, Reference to the plugin managing the GUI (mandatory)

#define MUIM_Play2CPC_Notify (TAG_USER | 0x0100) // struct MUIP_Play2CPC_Notify, notify the plugin or the GUI about changes
    #define MUIV_Play2CPC_Notify_ToGUI    0 // Notify the GUI
    #define MUIV_Play2CPC_Notify_ToPlugin 1 // Notify the plugin
        // For both directions
        #define MUIV_Play2CPC_Notify_CartridgeFileName 10 // Notify about the cartridge file name to use

struct MUIP_Play2CPC_Notify
{
    IPTR id;
    IPTR direction;
    IPTR type;
    CONST_STRPTR fileName;
};

struct Play2CPCData
{
    struct ACEpansionPlugin *plugin;

    Object *STR_Cartridge;
};



/*
** MCC_Play2CPCClass methods
*/
static IPTR mNotify(struct IClass *cl, Object *obj, struct MUIP_Play2CPC_Notify *msg)
{
    struct Play2CPCData *data = INST_DATA(cl,obj);

    switch(msg->direction)
    {
        case MUIV_Play2CPC_Notify_ToGUI:
            switch(msg->type)
            {
                case MUIV_Play2CPC_Notify_CartridgeFileName:
                    nnset(data->STR_Cartridge, MUIA_String_Contents, msg->fileName);
                    break;
            }
            break;

        case MUIV_Play2CPC_Notify_ToPlugin:
            switch(msg->type)
            {
                case MUIV_Play2CPC_Notify_CartridgeFileName:
                    Plugin_SetCartridgeFileName(data->plugin, msg->fileName);
                    break;
            }
            break;
    }

    return 0;
}

static IPTR mNew(struct IClass *cl, Object *obj, Msg msg)
{

    struct TagItem *tags,*tag;
    struct Play2CPCData *data;

    struct ACEpansionPlugin *plugin = NULL;

    Object *STR_Cartridge;
    Object *BTN_Eject;

    // Parse initial taglist
    for(tags=((struct opSet *)msg)->ops_AttrList; (tag=NextTagItem(&tags)); )
    {
        switch(tag->ti_Tag)
        {
            case MUIA_Play2CPC_Plugin:
                plugin = (struct ACEpansionPlugin *)tag->ti_Data;
                break;                              
        }
    }

    if(!plugin) return 0;

    obj = (Object *)DoSuperNew(cl,obj,
        GroupFrameT(GetString(MSG_PREFS)),
        MUIA_Group_Columns, 2,
        Child, Label2(GetString(MSG_CARTRIDGE)),
        Child, HGroup,
            Child, PopaslObject,
                MUIA_ShortHelp, GetString(MSG_CARTRIDGE_HELP),
                MUIA_Popstring_String, STR_Cartridge = StringObject,
                    StringFrame,
                    MUIA_String_MaxLen, 512,
                    MUIA_String_SpellChecking, FALSE,
                    MUIA_CycleChain, TRUE,
                    End,
                MUIA_Popstring_Button, TextObject,
                    ButtonFrame,
                    MUIA_Weight, 0,
                    MUIA_InputMode , MUIV_InputMode_RelVerify,
                    MUIA_Background, MUII_ButtonBack,
                    MUIA_CycleChain, TRUE,
                    MUIA_Text_Contents, "\33I[6:19]", // MUII_PopFile
                    End,
                ASLFR_TitleText, GetString(MSG_ASL_REQUEST),
                ASLFR_DoPatterns, TRUE,
                ASLFR_InitialPattern, "#?.(cpr|xpr)",
                End,
            Child, BTN_Eject = TextObject,
                ButtonFrame,
                MUIA_Weight, 0,
                MUIA_InputMode , MUIV_InputMode_RelVerify,
                MUIA_Background, MUII_ButtonBack,
                MUIA_CycleChain, TRUE,
                MUIA_Text_Contents, GetString(MSG_EJECT_BUTTON),
                End,
            End,
        TAG_MORE,((struct opSet *)msg)->ops_AttrList);

    if(!obj) return 0;

    data = INST_DATA(cl,obj);

    data->plugin = plugin;

    data->STR_Cartridge = STR_Cartridge;

    DoMethod(STR_Cartridge, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime,
             obj, 4, MUIM_Play2CPC_Notify, MUIV_Play2CPC_Notify_ToPlugin,
                     MUIV_Play2CPC_Notify_CartridgeFileName, MUIV_TriggerValue);
    DoMethod(BTN_Eject, MUIM_Notify, MUIA_Pressed, FALSE,
             obj, 4, MUIM_Play2CPC_Notify, MUIV_Play2CPC_Notify_ToPlugin,
                     MUIV_Play2CPC_Notify_CartridgeFileName, NULL);

    return (IPTR)obj;
}



/*
** MUI Custom Class dispatcher, creation & destruction
*/
DISPATCHER(Play2CPCClass)
{
    switch (msg->MethodID)
    {
        case OM_NEW               : return(mNew   (cl,obj,(APTR)msg));
        case MUIM_Play2CPC_Notify : return(mNotify(cl,obj,(APTR)msg));
    }
}
DISPATCHER_END



/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
    UtilityBase = OpenLibrary("utility.library", 0L);
    IntuitionBase = OpenLibrary("intuition.library", 0L);
    MUIMasterBase = OpenLibrary("muimaster.library", 0L);

    if(UtilityBase && IntuitionBase && MUIMasterBase)
    {
        MCC_Play2CPCClass = MUI_CreateCustomClass(
            NULL,
            MUIC_Group,
            NULL,
            sizeof(struct Play2CPCData),
            DISPATCHER_REF(Play2CPCClass));
    }

    return MCC_Play2CPCClass != NULL;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
    if(MCC_Play2CPCClass)
        MUI_DeleteCustomClass(MCC_Play2CPCClass);

    CloseLibrary(MUIMasterBase);
    CloseLibrary(IntuitionBase);
    CloseLibrary(UtilityBase);
}



/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object * GUI_Create(struct ACEpansionPlugin *plugin)
{
    return Play2CPCObject,
        MUIA_Play2CPC_Plugin, plugin,
        End;
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
    MUI_DisposeObject(gui);
}



/*
** Functions called from the plugin
*/
VOID GUI_SetCartridgeFileName(Object *gui, CONST_STRPTR fileName)
{
    DoMethod(gui, MUIM_Play2CPC_Notify, MUIV_Play2CPC_Notify_ToGUI,
             MUIV_Play2CPC_Notify_CartridgeFileName, fileName);
}

VOID GUI_NotifyCartridgeLoadError(UNUSED Object *gui, CONST_STRPTR fileName)
{
    MUI_Request(NULL, NULL, 0, GetString(MSG_TITLE), GetString(MSG_OK), GetString(MSG_CARTRIDGE_LOAD_FAILED_S), fileName);
}

VOID GUI_NotifyCartridgeSaveError(UNUSED Object *gui, CONST_STRPTR fileName)
{
    MUI_Request(NULL, NULL, 0, GetString(MSG_TITLE), GetString(MSG_OK), GetString(MSG_CARTRIDGE_SAVE_FAILED_S), fileName);
}

