/*
** Graphical User Interface generic header
**
** interface_mui.c
**      Implementation using the Magic User Interface
**      for MorphOS, AmigaOS PPC, AmigaOS 68K and AROS
*/

#ifndef ACEPANSION_GUI_H
#define ACEPANSION_GUI_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef INTUITION_CLASSUSR_H
#include <intuition/classusr.h>
#endif
#ifndef ACEPANSION_PLUGIN_H
#include <acepansion/plugin.h>
#endif


/*
** GUI generic API
*/
BOOL GUI_InitResources(VOID);
VOID GUI_FreeResources(VOID);

Object * GUI_Create(struct ACEpansionPlugin *plugin);
VOID GUI_Delete(Object *gui);


/*
** Play2CPC GUI specific API to be called from the plugin
*/
VOID GUI_SetCartridgeFileName(Object *gui, CONST_STRPTR fileName);
VOID GUI_NotifyCartridgeLoadError(Object *gui, CONST_STRPTR fileName);
VOID GUI_NotifyCartridgeSaveError(Object *gui, CONST_STRPTR fileName);


#endif /* ACEPANSION_GUI_H */

