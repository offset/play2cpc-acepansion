## Version $VER: play2cpc.acepansion.catalog 1.0 (02.10.2022)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG deutsch "Release/Catalogs/deutsch/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Plug a Play2CPC
Brancher une Play2CPC
Play2CPC anschlie�en
Conecte un Play2CPC
;
MSG_MENU_PREFS
Play2CPC preferences...
Pr�f�rences de la Play2CPC...
Play2CPC-Einstellungen ...
Preferencias de Play2CPC...
;
MSG_TITLE
Play2CPC
Play2CPC
Play2CPC
Play2CPC
;
MSG_PREFS
Preferences
Pr�f�rences
Einstellungen
Preferencias
;
MSG_CARTRIDGE
Cartridge:
Cartouche :
Modul:
Cartucho:
;
MSG_CARTRIDGE_HELP
Cartridge to plug into the Play2CPC.
Cartouche � brancher dans la Play2CPC.
Modul zum Anschluss an den Play2CPC.
Cartucho a conectar dentro de Play2CPC.
;
MSG_ASL_REQUEST
Select the cartridge file to use...
S�lectionnez le fichier cartouche � utiliser...
W�hle die zu verwendende Moduldatei aus ...
Eliga el fichero cartucho a usar...
;
MSG_CARTRIDGE_LOAD_FAILED_S
\ecImpossible to load cartridge\n"%s".
\ecImpossible de lire la cartouche\n� %s �.
\ecUnm�glich Modul �%s� zu laden.
\ecImposible cargar el cartucho\n� %s �.
;
MSG_CARTRIDGE_SAVE_FAILED_S
\ecImpossible to save modification to cartridge\n"%s".
\ecImpossible de sauver les modifications de la cartouche\n� %s �.
\ecEs ist unm�glich, die �nderung auf dem Modul\n�%s� zu speichern.
\ecImposible guardar las modificaciones en el cartucho\n� %s �.
;
MSG_OK
  Ok\x20\x20
  Ok\x20\x20
  Ok\x20\x20
  Ok\x20\x20
;
MSG_EJECT_BUTTON
  Eject\x20\x20
  �jecter\x20\x20
  Auswerfen\x20\x20
  Expulsar\x20\x20
;
